package truman.testbed.springapp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringHibernateDemo {
    public static void main(String[] args) {
	ApplicationContext context = new ClassPathXmlApplicationContext("beans-config.xml");

	IUserDAO userDAO = (IUserDAO) context.getBean("userDAO");

	User user = new User();
	user.setAge(1);
	user.setName("小咪");
	userDAO.insert(user);

	user = userDAO.find(new Integer(15));

	System.out.println("name: " + user.getName());
    }
}
