package truman.testbed.springapp;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateDemo {
    private static Logger logger = Logger.getLogger(HibernateDemo.class);

    public static void main(String[] args) {

	Configuration config = new Configuration().configure();
	SessionFactory sessionFactory = config.buildSessionFactory();
	IDrvCountryCodeDAO userDAO = new DrvCountryCodeDAO(sessionFactory);

	DrvCountryCode user = new DrvCountryCode();
	user.setCountryCode("992");
	user.setCountryName("三民主義統一中國");

	userDAO.insert(user);

	logger.log(Level.INFO, "querying object.");
	DrvCountryCode country = userDAO.find("992");

	System.out.println("name: " + country.getCountryName());
    }
}
