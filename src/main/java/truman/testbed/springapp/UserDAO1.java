package truman.testbed.springapp;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class UserDAO1 implements IUserDAO {
	private HibernateTemplate hibernateTemplate;
    //private SessionFactory sessionFactory; 
    
    public UserDAO1() {
    }
    
    public UserDAO1(SessionFactory sessionFactory) { 
        this.setSessionFactory(sessionFactory);
    }
    
    public void setSessionFactory(SessionFactory sessionFactory) { 
        hibernateTemplate=new HibernateTemplate(sessionFactory);
    } 
    
    public void insert(User user) {
     hibernateTemplate.save(user);
    }

    public User find(Integer id) {
        User user = (User) hibernateTemplate.get(User.class, id);
       
        return user;
    }
}
