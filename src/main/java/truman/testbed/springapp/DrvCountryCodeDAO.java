package truman.testbed.springapp;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DrvCountryCodeDAO implements IDrvCountryCodeDAO {
    private SessionFactory sessionFactory;

    public DrvCountryCodeDAO() {
    }

    public DrvCountryCodeDAO(SessionFactory sessionFactory) {
	this.setSessionFactory(sessionFactory);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    public void insert(DrvCountryCode user) {
	// ��oSession
	Session session = sessionFactory.openSession();
	// �}�ҥ��
	Transaction tx = session.beginTransaction();
	// �����x�s����
	session.save(user);
	// �e�X���
	tx.commit();
	session.close();
    }

    public DrvCountryCode find(String id) {
	Session session = sessionFactory.openSession();

	DrvCountryCode user = (DrvCountryCode) session.load(DrvCountryCode.class, id);
	Hibernate.initialize(user);

	session.close();

	return user;
    }
}
