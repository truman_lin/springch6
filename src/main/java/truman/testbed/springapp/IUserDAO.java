package truman.testbed.springapp;

public interface IUserDAO {
    public void insert(User user);
    public User find(Integer id);
}
