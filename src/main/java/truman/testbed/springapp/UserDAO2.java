package truman.testbed.springapp;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class UserDAO2 extends HibernateDaoSupport implements IUserDAO {

    public void insert(User user) {
     getHibernateTemplate().save(user);
    }

    public User find(Integer id) {
        User user = (User) getHibernateTemplate().get(User.class, id);
       
        return user;
    }
}
