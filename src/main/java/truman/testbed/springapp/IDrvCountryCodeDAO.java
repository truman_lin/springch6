package truman.testbed.springapp;

public interface IDrvCountryCodeDAO {
    public void insert(DrvCountryCode user);

    public DrvCountryCode find(String id);
}
